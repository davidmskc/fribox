window.addEventListener('load', function() {
	//stran nalozena
	
	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	}
	
	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	}
	
	document.querySelector("#nalozi").addEventListener("click", prizgiCakanje);
	
	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function() { 
		prizgiCakanje();
		var xhttp = new XMLHttpRequest(); //AJAX klic na strežnik, stran se naloži, v ozadju pa js še kliče 
										  //objekt xhttp, nato povemo, kaj se zgodi
		
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) { //če je vse OK:
				var datoteke = JSON.parse(xhttp.responseText); //html element id=datoteke, dodamo te, ki jih imamo v mapi
				
				var datotekeHTML = document.querySelector("#datoteke");
				
				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];
					
					var velikost = datoteka.velikost;
					var enota = "B";
					
					if(velikost > 1024  ) { velikost = Math.round(velikost/1024); enota = "KiB"; } //veriga korakov, dokler je velikost še ustrezna pogojem
					if(velikost > 1024  ) { velikost = Math.round(velikost/1024); enota = "MiB"; }
					if(velikost > 1024  ) { velikost = Math.round(velikost/1024); enota = "GiB"; }
					
					datotekeHTML.innerHTML += " \
						<div class='datoteka senca rob'> \
							<div class='naziv_datoteke'> " + datoteka.datoteka + "  (" + velikost + " " + enota + ") </div> \
							<div class='akcije'> \
							<span> <a href='/poglej/" + datoteka.datoteka + "' target = '_BLANK'> Poglej</a> </span>  \
							| <span><a href='/prenesi/" + datoteka.datoteka + "' target='_self'>Prenesi</a></span> \
							| <span akcija='brisi' datoteka='"+ datoteka.datoteka +"'>Izbriši</span> </div> \
					    </div>";	
				}
				
				if (datoteke.length > 0) {
					//document.querySelector("span[akcija=brisi]").addEventListener("click", brisi); //dobimo le prvi zadetek
					var brisanje = document.querySelectorAll("span[akcija=brisi]"); //dobimo vse zadetke
					for(var i = 0; i < brisanje.length; i++) {
						brisanje[i].addEventListener("click", brisi);
					}
				}
				ugasniCakanje();
			}
		};
		xhttp.open("GET", "/datoteke",true); //poklicemo streznik z datoteke potjo, true pomeni asinhrono, da se stran ne refresha
		xhttp.send(); //poslje na stran
	}
	
	pridobiSeznamDatotek(); //klicemo funkcijo
	
	var brisi = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				} else {
					alert("Datoteke ni bilo možno izbrisati!");
				}
			}
			ugasniCakanje();
		};
		xhttp.open("GET", "/brisi/"+this.getAttribute("datoteka"), true);
		xhttp.send();
	}

});